function palindrome(str) {
  const regxSpecial = /[^\W\s_]+/;
  let strArr = str
                .toLowerCase()
                .split('')
                .filter((char) => regxSpecial.test(char));
                
  for(let i = 0; i < Math.round(strArr.length / 2); i++) {
    if(strArr[i] !== strArr[strArr.length - i - 1]) return false;
  }
  return true;

}

palindrome("eye");