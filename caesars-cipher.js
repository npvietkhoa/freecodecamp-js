const alphabet = "thequickbrownfoxjumpsoverthelazydog";

function rot13(str) {
  const dictionary = dict(alphabet);
  return str.split('')
            .map((char) => shift(char, dictionary, 13))
            .join('');
}
function shift(char, dict, cipher) {
  let regexSpecial = /[\W\s_]/;
  if (regexSpecial.test(char) || !dict.includes(char)) return char;

  const charCode = (dict.indexOf(char) + cipher) % dict.length;

  return dict[charCode];  
}

function dict(alphabet) {
  let uniqueAlphabet = [...new Set(alphabet.toUpperCase().split(''))];
  return uniqueAlphabet.sort((a,b) => a.charCodeAt(0) - b.charCodeAt(0))
}


rot13("SERR PBQR PNZC");
