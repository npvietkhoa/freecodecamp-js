function convertToRoman(num) {
  const romanNumber = {
    1000: 'M',
    900: 'CM',
    500: 'D',
    400: 'CD',
    100: 'C',
    90: 'XC',
    50: 'L',
    40: 'XL',
    10: 'X',
    9: 'IX',
    5: 'V',
    4: 'IV',
    1: 'I'
  }
  const bases = Object.keys(romanNumber).sort((a, b) => b - a);
  let romanNum = '';

  while(num > 0) {
    for(let i in bases) {
      let rest = num % bases[i];
      if(rest < num || rest === 0) {
        let quo = Math.floor(num / bases[i]);
        romanNum += romanNumber[bases[i]].repeat(quo);
        num -=  quo * bases[i];
        break;
      }
    }
  }
  console.log(romanNum);
  return romanNum;
}
convertToRoman(30);
